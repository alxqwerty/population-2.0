using System;

namespace PopulationTask
{
    public static class Population
    {
        public static int GetYears(int initialPopulation, double percent, int visitors, int currentPopulation)
        {
            int years = 0;

            if (initialPopulation <= 0)
            {
                throw new ArgumentException("Initial population cannot be less or equals zero.", nameof(initialPopulation));
            }

            if (visitors <= 0)
            {
                throw new ArgumentException("Count of visitors cannot be less zero.", nameof(visitors));
            }

            if (currentPopulation <= 0)
            {
                throw new ArgumentException("Current population cannot be less or eqauls zero.", nameof(currentPopulation));
            }

            if (currentPopulation < initialPopulation)
            {
                throw new ArgumentException("Current population cannot be less than initial population.", nameof(currentPopulation));
            }

            if (percent < 0 || percent > 100)
            {
                throw new ArgumentOutOfRangeException(nameof(percent), "Value of percents cannot be less then 0% or more then 100%.");
            }

            while (initialPopulation < currentPopulation)
            {
                initialPopulation = initialPopulation + visitors + (int)(initialPopulation * percent / 100);
                years++;
            }

            return years;
        }
    }
}